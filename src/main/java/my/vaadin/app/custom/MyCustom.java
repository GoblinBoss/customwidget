package my.vaadin.app.custom;

import com.vaadin.shared.MouseEventDetails;
import my.vaadin.app.custom.client.customExistWidget.MyCustomClientRpc;
import my.vaadin.app.custom.client.customExistWidget.MyCustomServerRpc;
import my.vaadin.app.custom.client.customExistWidget.MyCustomState;
import com.vaadin.ui.AbstractComponent;

public class MyCustom extends AbstractComponent {
    MyCustomServerRpc rpc = new MyCustomServerRpc(){
        private int clickCount = 0;

        @Override
        public void clicked(MouseEventDetails mouseEventDetails) {
            if (++clickCount % 5 == 0) {
                getRpcProxy(MyCustomClientRpc.class).alert(
                        "Ok, that's enough!");
            }
            // update shared state
            getState().text = "You have clicked a lambda powered client side widget " + clickCount + " times";
        }
    };

    public MyCustom() {
        registerRpc(rpc);
        getState().text = "start";
    }

    @Override
    protected MyCustomState getState() {
        return (MyCustomState) super.getState();
    }
}
