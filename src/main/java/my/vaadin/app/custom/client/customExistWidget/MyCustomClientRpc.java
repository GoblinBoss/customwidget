package my.vaadin.app.custom.client.customExistWidget;

import com.vaadin.shared.communication.ClientRpc;

public interface MyCustomClientRpc extends ClientRpc {
    void alert(String message);
}
