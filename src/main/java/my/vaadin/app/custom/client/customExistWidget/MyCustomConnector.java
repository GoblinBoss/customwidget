package my.vaadin.app.custom.client.customExistWidget;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.vaadin.client.MouseEventDetailsBuilder;
import com.vaadin.client.communication.StateChangeEvent;
import com.vaadin.shared.MouseEventDetails;
import my.vaadin.app.custom.MyCustom;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.client.communication.RpcProxy;
import com.vaadin.client.ui.AbstractComponentConnector;
import com.vaadin.shared.ui.Connect;

@Connect(MyCustom.class)
public class MyCustomConnector extends AbstractComponentConnector {
    private final MyCustomServerRpc rpc = RpcProxy.create(MyCustomServerRpc.class, this);

    public MyCustomConnector() {
        registerRpc(MyCustomClientRpc.class, new MyCustomClientRpc() {
            @Override
            public void alert(String message) {
                Window.alert(message);
            }
        });

        getWidget().button.addClickHandler((ClickEvent event) -> {
            final MouseEventDetails mouseDetails = MouseEventDetailsBuilder
                    .buildMouseEventDetails(event.getNativeEvent(),
                            getWidget().button.getElement());
            rpc.clicked(mouseDetails);
        });
    }

    @Override
    protected Widget createWidget() {
        return GWT.create(MyCustomWidget.class);
    }

    @Override
    public MyCustomWidget getWidget() {
        return (MyCustomWidget) super.getWidget();
    }

    @Override
    public MyCustomState getState() {
        return (MyCustomState) super.getState();
    }

    @Override
    public void onStateChanged(StateChangeEvent stateChangeEvent) {
        super.onStateChanged(stateChangeEvent);

        // TODO: do something useful
        final String text = getState().text;
        getWidget().label.setText(text);
    }
}
