package my.vaadin.app.custom.client.customExistWidget;

import com.vaadin.shared.MouseEventDetails;
import com.vaadin.shared.communication.ServerRpc;

public interface MyCustomServerRpc extends ServerRpc {
    void clicked(MouseEventDetails mouseEventDetails);
}
