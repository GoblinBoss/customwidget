package my.vaadin.app.custom.client.customExistWidget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;

public class MyCustomWidget extends Composite {

    @UiField
    public Label label;
    @UiField
    public Button button;

    interface MyCustomWidgetUiBinder extends UiBinder<HTMLPanel, MyCustomWidget> {
    }

    private static MyCustomWidgetUiBinder ourUiBinder = GWT.create(MyCustomWidgetUiBinder.class);

    public MyCustomWidget() {
        initWidget(ourUiBinder.createAndBindUi(this));
    }
}